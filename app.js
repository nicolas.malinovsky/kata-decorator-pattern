const documentService = require("./doc-service");
const { WithCache } = require("./WithCache");

const documents = new WithCache(documentService);

documents.getDoc("1").then((doc) => {
    console.log(doc);
    documents.getDoc("1").then((doc) => {
        console.log(doc);

    });
});
