const axios = require("axios");

const URL = "https://62fe254741165d66bfb969ab.mockapi.io/api/v1/documents/";

const getDoc = async (id) => {
  const response = await axios.get(URL + id);
  return response.data;
};

const getAllDocs = async () => {
    const response = await axios.get(URL);
    return response.data;
};

module.exports = { getDoc, getAllDocs };
