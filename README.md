# Decorator Pattern - Cached Paging

Haga de cuenta que usará una biblioteca de terceros para descargar documentos de un servicio en la web.
Esta biblioteca, por diversos motivos, no puede ser extendida haciendo uso de herencia ni por ningún otro mecanismo provisto por el lenguaje.

## Kata - Parte 1

Actualmente la biblioteca doc-service expone dos métodos:

* getDoc (id): Document
* getAllDocs (): Array`<Document>`

Donde

```
Document  
{
    text: (String)
    id: (String)
}
```

Es necesario que extienda la funcionalidad de esta biblioteca para que mediante el uso de un nuevo método sea posible descargar páginas de 7 documentos como máximo.

Puntos a tener en cuenta:

* La página a descargar debe ser identificada con un entero, comenzando por el 1 (primera página).

* Si al intentar acceder a la última página no llegara a contener 7 documentos, debe retornar menos.

* Si al intentar acceder a una página inexistente, dada la cantidad de documentos almacenada en el servicio, el array devuelto debe estar vació.

* Los documentos no deben perder el órden con el que retornan del servicio.

**IMPORTANTE**

**Los tests existentes deben pasar, no pueden ser modificados y, en caso necesario, pueden agregarse tests extra**

---

## Kata - Parte 2

Contando con la funcionalidad anterior, es momento de optimizar recursos: tiempo de respuesta, consultas al servicio, etc.

Para cumplir con este atributo de calidad vamos a implementar un sistema de caché

De igual manera que lo realizado en la kata 1, extienda la funcionalidad del decorador para que cuente con una caché de documentos.

Puntos a tener en cuenta:

* La caché sólo será de descarga. Por el momento, no importa la carga.
* Se considera que los documentos almacenados en el servicio son estáticos, no cambian.

**IMPORTANTE**

**Los tests existentes deben pasar, no pueden ser modificados y, en caso necesario, pueden agregarse tests extra**
