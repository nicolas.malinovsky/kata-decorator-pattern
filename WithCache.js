class WithCache {
    constructor(service) {
        this.service = service;
        this.cache = {};
    }
}

module.exports = { WithCache };