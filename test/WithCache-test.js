const chai = require('chai')
const spies = require('chai-spies');
const expect = chai.expect;
const documentService = require('../doc-service');
const { WithCache } = require('../WithCache');
const WithPages = require('../WithPages');
chai.use(spies);


describe('Cache test', () => {
    it('should return the same document and getDoc against service should have been called once', async () => {
        const getDocSpy = chai.spy.on(documentService, 'getDoc');
        const documentsCache = new WithCache(documentService);
        const doc = await documentsCache.getDoc("1");
        expect(doc.id).to.deep.equal("1");
        const doc2 = await documentsCache.getDoc("1");
        expect(doc2.id).to.deep.equal("1");
        expect(getDocSpy).to.have.been.called.once;
    });
    it('should return the same page and getAllDocs against service should have been called once', async () => {
        const getAllDocseSpy = chai.spy.on(documentService, 'getAllDocs');
        const documentsCache = new WithCache(new WithPages(documentService));
        const page = await documentsCache.getPage(1);
        const page2 = await documentsCache.getPage(1);
        expect(page).to.deep.equal(page2);
        expect(getAllDocseSpy).to.have.been.called.once;
    });
});
