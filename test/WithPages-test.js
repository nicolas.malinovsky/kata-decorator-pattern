var expect = require('chai').expect;
let WithPages = require('../WithPages');
let documentservice = require('../doc-service');

describe('Pagination test', function () {

    it('Passing page 1 should return 7 elements', async function () {
        let pager = new WithPages(documentservice);
        let pages = await pager.getPage(1);
        expect(pages.length).equal(7);
    })
    it('Passing page 1 should return elements with ids 1 to 7', async function () {
        let pager = new WithPages(documentservice);
        let pages = await pager.getPage(1);
        let ids = pages.map(x => x.id);
        expect(ids).deep.equal(Array.from({ length: 7 }, (_, i) => (i + 1).toString()));
    })
    it('Passing page 8 should return only one doc with id 50', async function () {
        let pager = new WithPages(documentservice);
        let pages = await pager.getPage(8);
        let ids = pages.map(x => x.id);
        expect(ids).deep.equal(['50']);
    })
    it('getDocs should return 50 docs with ids 1 to 50', async function () {
        let pager = new WithPages(documentservice);
        let docs = await pager.getAllDocs();
        let ids = docs.map(x => x.id);
        expect(ids).deep.equal(Array.from({ length: 50 }, (_, i) => (i + 1).toString()));
    })
    it('Passing unexisting page should return empty array', async function () {
        let pager = new WithPages(documentservice);
        let docs = await pager.getPage(9);
        let ids = docs.map(x => x.id);
        expect(ids).deep.equal([]);
    })
})